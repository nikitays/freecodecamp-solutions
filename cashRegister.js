const checkCashRegister = (price, cash, cid) => {
    const cashLib = [100, 20, 10, 5, 1, 0.25, 0.1, 0.05, 0.01];
    let due = cash - price;
    const copyCid = cid.map(function (item) {
        return [...item];
    });
    const cidReverse = [...cid.slice().reverse()];
    const cidReverseLength = cidReverse.length;
    const cidChange = {};

    while (due > 0) {
        const tempDue = due;
        for (let q = 0; q < cidReverseLength; q++) {
            if (due >= cashLib[q]) {
                if (cidReverse[q][1] >= cashLib[q]) {
                    cidReverse[q][1] = +(cidReverse[q][1] - cashLib[q]).toFixed(2);
                    due = +(due - cashLib[q]).toFixed(2);
                    if (cidChange[cidReverse[q][0]]) {
                        cidChange[cidReverse[q][0]] += cashLib[q];
                    } else {
                        cidChange[cidReverse[q][0]] = 0;
                        cidChange[cidReverse[q][0]] += cashLib[q];
                    }
                    break;
                }
            }
        }
        if (tempDue === due) {
            return {status: "INSUFFICIENT_FUNDS", change: []};
        }
    }

    const isClearArray = Object.values(Object.fromEntries(cidReverse)).every(
        (elem) => elem === 0
    );

    if (isClearArray) {
        console.log({
            status: "CLOSED",
            change: copyCid,
        });
        return {
            status: "CLOSED",
            change: copyCid,
        };
    }

    return {
        status: "OPEN",
        change: Object.entries(cidChange),
    };
};

checkCashRegister(19.5, 20, [
    ["PENNY", 1.01],
    ["NICKEL", 2.05],
    ["DIME", 3.1],
    ["QUARTER", 4.25],
    ["ONE", 90],
    ["FIVE", 55],
    ["TEN", 20],
    ["TWENTY", 60],
    ["ONE HUNDRED", 100],
]);

checkCashRegister(3.26, 100, [
    ["PENNY", 1.01],
    ["NICKEL", 2.05],
    ["DIME", 3.1],
    ["QUARTER", 4.25],
    ["ONE", 90],
    ["FIVE", 55],
    ["TEN", 20],
    ["TWENTY", 60],
    ["ONE HUNDRED", 100],
]);

checkCashRegister(19.5, 20, [
    ["PENNY", 0.01],
    ["NICKEL", 0],
    ["DIME", 0],
    ["QUARTER", 0],
    ["ONE", 0],
    ["FIVE", 0],
    ["TEN", 0],
    ["TWENTY", 0],
    ["ONE HUNDRED", 0],
]);

checkCashRegister(19.5, 20, [
    ["PENNY", 0.01],
    ["NICKEL", 0],
    ["DIME", 0],
    ["QUARTER", 0],
    ["ONE", 1],
    ["FIVE", 0],
    ["TEN", 0],
    ["TWENTY", 0],
    ["ONE HUNDRED", 0],
]);

checkCashRegister(19.5, 20, [
    ["PENNY", 0.5],
    ["NICKEL", 0],
    ["DIME", 0],
    ["QUARTER", 0],
    ["ONE", 0],
    ["FIVE", 0],
    ["TEN", 0],
    ["TWENTY", 0],
    ["ONE HUNDRED", 0],
]);