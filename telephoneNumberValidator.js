function telephoneCheck(str) {
    const reg = /^(1|)( |)(\([0-9]{3}\)|[0-9]{3})(-| |)[0-9]{3}(-| |)[0-9]{4}$/
    return reg.test(str)
}

telephoneCheck("1 555-555-5555");