function rot13(str) {
    const string = str.split("");
    const before = "ABCDEFGHIJKLM".split("");
    const after = "NOPQRSTUVWXYZ".split("");
    const res = string.map((word) => {
        const bef = before.indexOf(word);
        const aft = after.indexOf(word);
        if (bef !== -1) {
            return after[bef];
        }
        if (aft !== -1) {
            return before[aft];
        }
        return word;
    })
    return res.join("");
}

rot13("SERR PBQR PNZC");